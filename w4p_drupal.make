api = 2
core = 7.x

projects[] =  drupal

;Themes
projects[boron][type] = "theme"
projects[boron][directory_name] = "boron"
projects[boron][download][type] = "git"
projects[boron][download][url] = "https://GitWEB4PRO@bitbucket.org/GitWEB4PRO/drupal-theme.git"

;Modules
projects[views][subdir] = contrib
projects[pathauto][subdir] = contrib
projects[admin_menu][subdir] = contrib
projects[ckeditor][subdir] = contrib
projects[ckeditor_link][subdir] = contrib
projects[ocupload][subdir] = contrib
projects[filefield_sources][subdir] = contrib
projects[google_analytics][subdir] = contrib
projects[metatag][subdir] = contrib
projects[imce][subdir] = contrib

;Modules dependencies
projects[token][subdir] = contrib
projects[ctools][subdir] = contrib

;Librbaries
libraries[ckeditor][download][type] = get
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.5.9/ckeditor_4.5.9_standard.zip"
libraries[ckeditor][destination] = libraries
libraries[ckeditor][directory_name] = ckeditor

;Profile
projects[w4p_profile][type] = "profile"
projects[w4p_profile][download][type] = "git"
projects[w4p_profile][download][url] = "https://GitWEB4PRO@bitbucket.org/GitWEB4PRO/w4p_profile.git"
projects[w4p_profile][download][branch] = "profile"